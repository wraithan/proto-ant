use bevy::{
    diagnostic::{Diagnostics, FrameTimeDiagnosticsPlugin},
    prelude::*,
    render::camera::Camera,
};
use bevy_egui::{egui, EguiContext};

use crate::ant::{create_ant_bundle, Ant};

pub fn run_ui(
    mut commands: Commands,
    egui_context: ResMut<EguiContext>,
    ant_query: Query<&Ant>,
    diagnostics: Res<Diagnostics>,
    camera_transform: Query<&Transform, With<Camera>>,
) {
    egui::SidePanel::left("left_info_panel", 150.0).show(egui_context.ctx(), |ui| {
        ui.set_min_width(150.0);
        ui.heading("Info Panel");

        ui.separator();

        ui.horizontal(|ui| {
            if ui.button("Spawn Ant").clicked() {
                for _ in 0..1000 {
                    commands.spawn_bundle(create_ant_bundle()).insert(Ant);
                }
            }
        });

        ui.horizontal(|ui| {
            ui.label(format!("Ant count: {}", ant_query.iter().len()));
        });

        ui.separator();

        egui::Grid::new("diagnostics").striped(true).show(ui, |ui| {
            for diagnostic in diagnostics.iter() {
                if let Some(value) = diagnostic.value() {
                    ui.label(format!("{}: ", diagnostic.name));
                    if diagnostic.id == FrameTimeDiagnosticsPlugin::FRAME_TIME {
                        ui.label(format!(
                            "{:.4}{:1}",
                            diagnostic.average().unwrap_or_default(),
                            diagnostic.suffix
                        ));
                    } else {
                        ui.label(format!("{:4.2}{:1}", value, diagnostic.suffix));
                    }
                    ui.end_row();
                }
            }
        });
    });

    for transform in camera_transform.iter() {
        egui::Window::new("Camera").show(egui_context.ctx(), |ui| {
            ui.label(format!("Camera Transform: {:#?}", transform));
        });
    }
}
