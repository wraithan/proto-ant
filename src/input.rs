use bevy::prelude::*;

pub struct InputSettings {
    pub keyboard_pan_sensitivity: f32,
    pub keyboard_zoom_sensitivity: f32,
}

impl Default for InputSettings {
    fn default() -> Self {
        Self {
            keyboard_pan_sensitivity: 5.0,
            keyboard_zoom_sensitivity: 1.0,
        }
    }
}

pub enum CameraMovementEvent {
    Pan(Vec2),
    Zoom(f32),
}

pub fn input_translator(
    settings: Res<InputSettings>,
    keys: Res<Input<KeyCode>>,
    mut ev_camera: EventWriter<CameraMovementEvent>,
) {
    // Keyboard Pan
    {
        let mut camera_motion = Vec2::ZERO;
        if keys.pressed(KeyCode::W) {
            camera_motion.y += 1.0;
        }
        if keys.pressed(KeyCode::A) {
            camera_motion.x -= 1.0;
        }
        if keys.pressed(KeyCode::S) {
            camera_motion.y -= 1.0;
        }
        if keys.pressed(KeyCode::D) {
            camera_motion.x += 1.0;
        }
        camera_motion *= settings.keyboard_pan_sensitivity;
        if camera_motion != Vec2::ZERO {
            ev_camera.send(CameraMovementEvent::Pan(camera_motion));
        }
    }

    // Keyboard Zoom
    {
        let mut zoom_amount = 0.0;
        let zoom_in = keys.pressed(KeyCode::Q);
        let zoom_out = keys.pressed(KeyCode::E);
        if zoom_in ^ zoom_out {
            if zoom_in {
                zoom_amount = 1.1;
            } else {
                zoom_amount = 0.9
            }
        }

        zoom_amount *= settings.keyboard_zoom_sensitivity;

        if zoom_amount != 0.0 {
            ev_camera.send(CameraMovementEvent::Zoom(zoom_amount));
        }
    }
}
