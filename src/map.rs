use bevy::{
    prelude::*,
    reflect::TypeUuid,
    render::{
        mesh::shape,
        pipeline::{PipelineDescriptor, RenderPipeline},
        render_graph::{base, AssetRenderResourcesNode, RenderGraph},
        renderer::RenderResources,
        shader::ShaderStages,
    },
};

#[derive(Default)]
pub struct ShaderAssets {
    vert: Handle<Shader>,
    frag: Handle<Shader>,
    loaded: bool,
}

pub fn setup_assets(asset_server: ResMut<AssetServer>, mut shader_assets: ResMut<ShaderAssets>) {
    info!("Loading shaders");

    asset_server.watch_for_changes().unwrap();

    shader_assets.vert = asset_server.load::<Shader, _>("shaders/map.vert");
    shader_assets.frag = asset_server.load::<Shader, _>("shaders/map.frag");
    shader_assets.loaded = false;
}

pub fn shader_assets_ready(
    mut commands: Commands,
    asset_server: ResMut<AssetServer>,
    mut pipelines: ResMut<Assets<PipelineDescriptor>>,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<MapMaterial>>,
    mut render_graph: ResMut<RenderGraph>,
    mut shader_assets: ResMut<ShaderAssets>,
) {
    use bevy::asset::LoadState;

    if shader_assets.loaded {
        return;
    }

    if asset_server.get_load_state(shader_assets.vert.clone()) != LoadState::Loaded
        || asset_server.get_load_state(shader_assets.frag.clone()) != LoadState::Loaded
    {
        info!("Shaders still not loaded...");
        return;
    }

    info!("Shaders loaded, adding to render graph");

    let pipeline_handle = pipelines.add(PipelineDescriptor::default_config(ShaderStages {
        vertex: shader_assets.vert.clone(),
        fragment: Some(shader_assets.frag.clone()),
    }));

    render_graph.add_system_node(
        "map_material",
        AssetRenderResourcesNode::<MapMaterial>::new(true),
    );

    render_graph
        .add_node_edge("map_material", base::node::MAIN_PASS)
        .unwrap();

    let material = materials.add(MapMaterial {
        color: Color::rgb(0.0, 0.8, 0.0),
    });

    commands
        .spawn_bundle(MeshBundle {
            mesh: meshes.add(Mesh::from(shape::Quad {
                size: Vec2::new(1024.0 * 16.0, 10240.0 * 16.0),
                flip: false,
            })),
            render_pipelines: RenderPipelines::from_pipelines(vec![RenderPipeline::new(
                pipeline_handle,
            )]),
            transform: Transform::from_xyz(0.0, 0.0, 0.0),
            ..Default::default()
        })
        .insert(material);

    shader_assets.loaded = true;
}

#[derive(RenderResources, Default, TypeUuid)]
#[uuid = "24b07a40-4b6a-41fa-98ce-b609ed15800e"]
pub struct MapMaterial {
    pub color: Color,
}
