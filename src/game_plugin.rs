use bevy::{app::PluginGroupBuilder, prelude::*, render::camera::Camera};
use bevy_egui::EguiPlugin;
use bevy_prototype_lyon::prelude::*;

use crate::{
    gui::run_ui,
    input::{input_translator, CameraMovementEvent, InputSettings},
    map,
};

fn setup_camera_ortho(mut commands: Commands) {
    commands.spawn_bundle(OrthographicCameraBundle::new_2d());
}

fn update_camera(
    mut camera_transform: Query<&mut Transform, With<Camera>>,
    mut ev_camera: EventReader<CameraMovementEvent>,
) {
    for ev in ev_camera.iter() {
        match *ev {
            CameraMovementEvent::Pan(amount) => {
                for mut transform in camera_transform.iter_mut() {
                    transform.translation.x += amount.x;
                    transform.translation.y += amount.y;
                }
            }
            CameraMovementEvent::Zoom(amount) => {
                for mut transform in camera_transform.iter_mut() {
                    transform.scale *= amount;
                    if transform.scale.x < 1.0 {
                        transform.scale = Vec3::ONE;
                    }
                }
            }
        }
    }
}

fn setup_window(mut windows: ResMut<Windows>) {
    let window = windows
        .get_primary_mut()
        .expect("Couldn't find primary window");
    window.set_resolution(1324.0, 1024.0);
}

struct GamePlugin;

impl Plugin for GamePlugin {
    fn build(&self, app: &mut AppBuilder) {
        app.insert_resource(ClearColor(Color::rgb(0.0, 0.0, 0.0)))
            .init_resource::<InputSettings>()
            .init_resource::<map::ShaderAssets>()
            .add_asset::<map::MapMaterial>()
            .add_event::<CameraMovementEvent>()
            .add_startup_system(map::setup_assets.system())
            .add_startup_system(setup_camera_ortho.system())
            .add_startup_system(setup_window.system())
            .add_system(map::shader_assets_ready.system())
            .add_system(input_translator.system().label("input_translator"))
            .add_system(update_camera.system().after("input_translator"))
            .add_system(run_ui.system())
            .add_system(bevy::input::system::exit_on_esc_system.system());
    }
}

pub struct GamePlugins;

impl PluginGroup for GamePlugins {
    fn build(&mut self, group: &mut PluginGroupBuilder) {
        group.add(EguiPlugin);
        group.add(ShapePlugin);
        group.add(GamePlugin);
    }
}
