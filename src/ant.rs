use bevy::{
    math::Vec2,
    prelude::{Color, Transform},
};
use bevy_prototype_lyon::{entity::ShapeBundle, prelude::*};
use rand::prelude::*;
pub struct Ant;

pub fn create_ant_bundle() -> ShapeBundle {
    let head = shapes::Circle {
        radius: 13.0,
        center: Vec2::new(0.0, 23.0),
    };
    let mesosoma = shapes::Circle {
        radius: 10.0,
        center: Vec2::new(0.0, 0.0),
    };
    let gaster = shapes::Ellipse {
        radii: Vec2::new(12.0, 18.0),
        center: Vec2::new(0.0, -28.0),
    };

    let mut geo = GeometryBuilder::new();

    geo.add(&head);
    geo.add(&mesosoma);
    geo.add(&gaster);

    let mut rng = rand::thread_rng();

    let mut transform = Transform::from_xyz(
        rng.gen_range(-500.0..500.0),
        rng.gen_range(-500.0..500.0),
        1.0,
    );
    transform.scale *= 0.1;

    geo.build(
        ShapeColors::outlined(Color::ORANGE_RED, Color::ORANGE),
        DrawMode::Outlined {
            fill_options: FillOptions::default(),
            outline_options: StrokeOptions::default().with_line_width(1.0),
        },
        transform,
    )
}
