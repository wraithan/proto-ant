mod ant;
mod game_plugin;
mod gui;
mod input;
mod map;

use bevy::{
    diagnostic::{EntityCountDiagnosticsPlugin, FrameTimeDiagnosticsPlugin},
    prelude::*,
    wgpu::diagnostic::WgpuResourceDiagnosticsPlugin,
};

fn main() {
    App::build()
        .add_plugins(DefaultPlugins)
        .add_plugin(FrameTimeDiagnosticsPlugin::default())
        .add_plugin(EntityCountDiagnosticsPlugin)
        .add_plugin(WgpuResourceDiagnosticsPlugin)
        .add_plugins(game_plugin::GamePlugins)
        .run();
}
