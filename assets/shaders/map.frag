#version 450

layout(location = 0) out vec4 o_Target;

layout(set = 2, binding = 0) uniform MapMaterial_color {
    vec4 color;
};

void main() {
    o_Target = vec4(vec3(color) * 0.3, color.a);
}
