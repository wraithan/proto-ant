# Proto Ant

Let's make an ant sim!

## Game / Simulation Ideas

* Ant colony needs food to make more ants. 
* Food replenishes daily
* Some levels have threats
* Most threats are food if they die
* Pharomones guide the ants
* Multiple kinds of ants, not sure if they should be more "fixed" or more "DIY"
    * Fixed would have workers, soldiers, etc.
    * DIY would need a points system to allocate to traits and save them
* Prestige would be starting a new hive

## History / References

Around a decade ago I played in [aichallenge.org's ant challenge](https://github.com/aichallenge/aichallenge) which is no longer hosted but at least some source is there. Lately seeing various ant videos from folks, while I played a game called [Incremancer](https://incremancer.gti.nz/) which is an incremental zombie game, with lots of agents doing path finding. I thought it would be fun to try to make a game around ants or some other agents using pharomone trials, but also messing around with this incremental style.

### Videos:

* Sebastian Lague
    * [Coding Adventure: Ant and Slime Simlations](https://www.youtube.com/watch?v=X-iSQQgOd1A)
* Pezzza's Work
    * [C++ Ants Simulation](https://www.youtube.com/watch?v=81GQNPJip2Y&t=144s)
    * [C++ Ants Simulation 2, Path optimization](https://www.youtube.com/watch?v=emRXBr5JvoY)
    * [C++ Ants Simulation 3, Maze](https://www.youtube.com/watch?v=V1GeNm2D2DU)

## Known issues

* bevy
    * When minimizing it currently due to dividing the dimensions of the window and them being 0. 
        * https://github.com/bevyengine/bevy/issues/170
        * Error message:
        ```
        wgpu error: Validation Error

        Caused by:
            In Device::create_texture
            Dimension X is zero
        ```
    * Since adding assets to my demo test thing, it now crashes with release builds. The issue I found suggests there is a timing issue between asset load and asset read and additional code might solve it.
        * https://github.com/bevyengine/bevy/issues/1359
        * Error message:
        ```
        thread 'main' panicked at 'called `Option::unwrap()` on a `None` value', C:\Users\wraithan\.cargo\registry\src\github.com-1ecc6299db9ec823\bevy_render-0.5.0\src\pipeline\pipeline_compiler.rs:82:49
        note: run with `RUST_BACKTRACE=1` environment variable to display a backtrace
        error: process didn't exit successfully: `target\release\proto-ant.exe` (exit code: 0xc000041d)
        ```
        * I've since added code to handle this in commit 7f68c19ff1f5160a17e254c7baa90ae90792596f, could be more graceful but at least it doesn't crash
    * Linker gets mad at the number of objects. I had to change their "fast build settings" in `.cargo/config.toml` for Windows to turn off `share-generics=off`. This shouldn't happen anymore for me but may be needed for other platforms as I build this codebase there. Right now I've only tried on Windows.
        * https://github.com/bevyengine/bevy/issues/1110
        * Error message:
        ```
        LINK : fatal error LNK1189: library limit of 65535 objects exceeded

        error: aborting due to previous error

        error: could not compile `bevy_dylib
        ```